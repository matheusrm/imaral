<?php

class funcionario_co {

    private $autoLoad;
    private $mensagens;
    private $model;
    private $tabela = "funcionario";
    private $registros;

    public function __construct() {
        //$this->u = new Usuario_mo();
        $this->autoLoad = new Automacao();
        $this->model = new funcionario_mo();
        $this->registros['nm_class_mani'] = "funcionario";
    }

    public function Index($p = null) {
        $this->registros['listagem'] = $this->model->Listar_cliente();
        $this->registros['titulo'] = 'Funcionário';
        $this->autoLoad->AutoLoad($this->registros['nm_class_mani'] . '_Listar', $this->registros);
    }

    public function Erro($p = null) {
        //$this->autoLoad->AutoLoad("tela_erro");
        require_once 'pages/tela_erro.php';
    }

    public function Acao($p = null) {
        $this->registros['titulo'] = 'Funcionário';
        $this->registros['acao'] = $p[0];
        if ($this->registros['acao'] <> 'novo') {
            //Atribui o id do registro
            //var_dump($this->registros);

            isset($p[1]) ? $this->registros['idRegistro'] = $p[1] : $this->registros['idRegistro'] = null;
            //Faz o select do item
            $this->registros['idRegistro'] <> null ? $this->registros['dados'] = $this->model->Listar($this->registros['idRegistro']) : 0;
        }

        //Seleciona as classes de acessos
        //$ca = new Classe_acesso_mo();
        //$this->registros['classeacessoid'] = $ca->Listar(null, 'S');
        $this->autoLoad->AutoLoad($this->registros['nm_class_mani'] . '_acao', $this->registros);
    }

    public function Gravar($p = null) {
        $dados = filter_input_array(INPUT_POST, FILTER_DEFAULT);
      
        //var_dump($dados);
        
            $this->model->Gravar($dados);
        
      
          
    }

}
