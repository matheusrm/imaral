<?php

class Database {
    # Variável que guarda a conexão PDO.

    protected $db;
    protected $db_sem_bd;
    protected $erro = "sucesso";

    # Private construct - garante que a classe só possa ser instanciada internamente.

    public function __construct() {

        # Informações sobre o banco de dados:
        $db_host = "localhost";
        //$db_host = "192.168.0.38";
        $db_nome = "Imaral";
        $db_usuario = "root";
        $db_senha = '';
        $db_porta = '3306';


        $db_driver = "mysql";
        try {
            # Atribui o objeto PDO à variável $db.
            $this->db = new PDO("$db_driver:host=$db_host; port=$db_porta; dbname=$db_nome", $db_usuario, $db_senha);
            # Garante que o PDO lance exceções durante erros.
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            # Garante que os dados sejam armazenados com codificação UFT-8.
            // self::$db->exec('SET NAMES utf8');
        } catch (PDOException $e) {
            # Envia um e-mail para o e-mail oficial do sistema, em caso de erro de conexão.
            //   mail($sistema_email, "PDOException em $sistema_titulo", $e->getMessage());
            # Então não carrega nada mais da página.
            $this->erro = "Erro não tratado: " . $e->getMessage();
        }
    }

    public function __destruct() {
        return $this->db = null;
    }

    # Método estático - acessível sem instanciação.

    public function conexao() {
        /*
          # Garante uma única instância. Se não existe uma conexão, criamos uma nova.
          if (!self::$db) {
          new Database();
          }
          # Retorna a conexão.
         * 
         */
        return $this->db;
    }

    # Método estático - acessível sem instanciação.

    public function v_erro() {
        //$this->conexao();
        return $this->erro;
    }

    public function Begin() {
        $this->conexao();
        if ($this->conexao()->beginTransaction()) {
            return $this->db;
        }
        return $this->erro;
    }

    public function Commit() {
        $this->conexao()->Commit();
    }

    public function Rollback() {
        $this->conexao()->rollback();
    }

}

/*
  $pdo = new Database();
  $pdo->Beguin();
  var_dump($pdo->v_erro());

  /*
  $smtp = $pdo->prepare("SELECT * FROM admin.painel_usuario");
  if ($smtp->execute()) {
  $results = $smtp->fetchAll(PDO::FETCH_ASSOC);
  var_dump($results);
  } else {
  echo "erro";
  }
  var_dump(Database::v_erro());
 */
?>
