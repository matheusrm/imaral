<?php

class Delete extends Database {

    public function Deletar($conexao, $tabela, $condicao, $idCasa = null) {
        //$conexao = Database::conexao();

        /*$existe = array_search($tabela, TBL_S_USER, FALSE);
        if ($existe == FALSE) {
            // Verifica a condição
            $idUser = $_SESSION['itIdUser'];
        }

       /* //Verifica se a tabela é admin
        $existe = array_search($tabela, TBL_S_ADMIN, FALSE);
        var_dump($existe);*/
        
        
        $painel = "";
       
        ///Monta a condição
        $cond = "";
        $i2 = 0;
        $valores_cond;
        for ($i = 0; $i < count($condicao);) {
            $vId = explode('_', $condicao[$i]);
            $tab = "";
            if ($vId[0] == 'id' && isset($vId[1])) {
                $tab = '';
            }
            $cond = $cond . $tab . $condicao[$i++] . " " . $condicao[$i++] . " ? ";

            $valores_cond[$i2] = $condicao[$i++];
            if (count($condicao) != $i) {
                if (isset($cond[$i])) {
                    $cond = $cond . $condicao[$i++] . " ";
                }
                $i2++;
            }
        }


        $query = "DELETE FROM " . $painel . $tabela . " WHERE " . $cond;
        //var_dump($valores_cond);
        //echo $query;

        try {
            $stmt = $conexao->prepare($query);
            if ($condicao != null) {
                $i2 = 1;
                for ($i = 0; $i < count($valores_cond);) {
                    $stmt->bindValue($i2, $valores_cond[$i]);
                    $i2++;
                    $i++;
                }
            }
            if ($stmt->execute()) {
                $results = $stmt->rowCount();
                return true;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            $log = new Log_mo();
            $log->Inserir("erro_delete", "pdo_03", $exc, $query);
            return false;
        }catch (PDOException $exc) {
            $log = new Log_mo();
            $log->Inserir("erro_delete", "pdo_03", $exc, $query);
            return false;
        }



        // return $query;
    }

}

?>