
$(document).ready(function () {
    var $url = window.location.href;
    $url = $url.split('/');

    var $url_atual = $url[0] + "//" + $url[2] + "/" + $url[3] + "/";

    $("#conteudo").on("click", '.abrirModal', function () {
        $('.btn-primary').attr("disabled", "true");
        var titulo = $(this).data("title");
        console.log(titulo);
        $('#modalFormulario #titleModal').html(titulo);
        var type = $(this).data("type");
        // var urlPost = $(this).data("url");
        ///Zera a barra
        $('#barraprogresso').css('width', '0%');
        $('#carregamento').html('0%');

        $("#modalFormulario #conteudoModal").html("<center><img src='" + $url_atual + "images/carregando.gif'></center>");
        $('#modalFormulario #conteudoModal').load($url_atual + 'modal/' + type);

    });

/// Modal de confirmação
    $("#conteudo").on("click", '.abrirModalConfirmacao', function () {
        console.log("Modal Confirmação");
        $('.btn-primary').attr("disabled", "true");
        var titulo = $(this).data("title");
        $('#modalConfirmacao #titleModal').html(titulo);
        var type = $(this).data("type");
        // var urlPost = $(this).data("url");

        console.log($url_atual);
        $("#modalConfirmacao #carregamento ").html("");
        $('#modalConfirmacao #conteudoModal').load($url_atual + 'modal/' + type);
        //$('#modalConfirmacao #conteudoModal').html(titulo);

    });


    $(".modal").on("click", '#fecharModal', function () {
        $('#conteudoModal').html(" ");

    });

/// Modal de preferencial
    $("#conteudo").on("click", '.abrirModalPreferencial', function () {
        console.log("modal Preferencial");
        $('.btn-primary').attr("disabled", "true");
        var titulo = $(this).data("title");
        $('#modalPreferencial #titleModal').html(titulo);
        var type = $(this).data("type");
        // var urlPost = $(this).data("url");

        console.log($url_atual);
        $("#modalPreferencial #carregamento ").html("");
        $('#modalPreferencial #conteudoModal').load($url_atual + 'modal/' + type);
        //$('#modalConfirmacao #conteudoModal').html(titulo);
    });

/// Modal de normal
    $("#conteudo").on("click", '.abrirModalNormal', function () {
        console.log("modal-notification");
        $('.btn-primary').attr("disabled", "true");
        var titulo = $(this).data("title");
        $('#modalNormal #titleModal').html(titulo);
        var type = $(this).data("type");
        // var urlPost = $(this).data("url");

        console.log($url_atual);
        $("#modalNormal #carregamento ").html("");
        $('#modalNormal #conteudoModal').load($url_atual + 'modal/' + type);
        //$('#modalConfirmacao #conteudoModal').html(titulo);
    });



///Envia os formularios de cadastro
    $("#conteudo").on("click", '#enviaForm', function () {
        var url = $("#url").val();

        $('#formulario').ajaxForm({
            type: "POST",
            url: url,
            contentType: false,
            cache: false,
            processData: false,
            success: function (r) {

                try {
                    var dados = JSON.parse(r);
                    $.each(dados, function (index, val) {

                        //$('#barraprogresso').css('width', '100%');
                        //$('#carregamento').html('100%');

                        if (val.TP === "A") {
                            alertify.warning(val.MSG);
                        }
                        if (val.TP === "S") {
                            alertify.success(val.MSG);
                            var url = window.location.href;
                            var res = url.split("#").join('');
                            if (val.ATU === 'TRUE') {
                                window.location = res;
                                $("#fecharModal").click();
                            }
                        }
                        if (val.TP === "E") {
                            alertify.error(val.MSG);
                        }
                    });
                } catch (e) {
                    alertify.error("Ocorreu um erro na atualização!");
                }


            }
        }).submit();
    });

///Envia os formularios do CKeditor
    $(".main_container").on("click", '#enviaCkeditor', function () {
        $("#conteudo").val(CKEDITOR.instances.conteudo2.getData());


        var url = $("#url").val();
        var da = new FormData($("form[id='formulario']")[0]);

        $.ajax({
            type: "POST",
            data: da,
            url: url,
            contentType: false,
            cache: false,
            processData: false,
            success: function (r) {
                try {
                    var dados = JSON.parse(r);
                    $.each(dados, function (index, val) {
                        //console.error(val.TP);
                        //console.error(dados.MSG);

                        $('#barraprogresso').css('width', '100%');
                        $('#carregamento').html('100%');

                        if (val.TP === "A") {
                            alertify.warning(val.MSG);
                            // console.error(val.MSG);
                        }
                        if (val.TP === "S") {
                            alertify.success(val.MSG);
                            var url = window.location.href;
                            var res = url.split("#").join('');
                            if (val.ATU === 'TRUE') {
                                window.location = res;
                                $("#fecharModal").click();
                            }
                        }
                        if (val.TP === "E") {
                            alertify.error(val.MSG);
                        }
                        //$('#carregamento').html("");
                    });
                } catch (e) {
                    alertify.error("Ocorreu um erro na atualização!");
                }


            }
        })
    });

    ///Modal de visualizar certificado
    $(".main_container").on("click", '#visualizarCertificado', function () {
        // alert('ok');
        $("#formulario").attr("action", $url_atual + "admin/post/geped/Visualizar_certificado_teste.ss");
    });

    /*datepicker
     $(".data").datepicker({
     format: "dd/mm/yyyy",
     language: "pt-BR"
     });
     */


});
// datable
var table = $('#datatable-buttons').DataTable();
table
        .column('0:visible')
        .order('desc')
        .draw();

var $url = window.location.href;
$url = $url.split('/');

var $url_atual = $url[0] + "//" + $url[2] + "/" + $url[3] + '/';
function buscarPoDisp(idArduino, tipoPorta, idElemento, acao, idItem) {

    $.ajax({
        //url: "php/buscaValorSensor.php",
        url: $url_atual + "post/arduino/Listar_p_disp/" + idArduino + "/" + tipoPorta + "/" + acao + "/" + idItem + "..html",
        success: function (r) {
            var dados = JSON.parse(r);
            console.log(r);
            $('#' + idElemento).append('<option value="-1"> Selecione</option>');
            $.each(dados, function (index, val) {
                $('#' + idElemento).append('<option value="' + val + '">' + val + '</option>');
            });
        }
    });
}


function addCondicao(qtdCondi, line, p_start, typeVal, typeText, valorId, valorNm) {


    //console.log(typeText);
    $('#condAdicionada').append('<div>');
    $('#condAdicionada').append('<table id="condicoes' + qtdCondi + '" class="condicoes" ><tr id="condTD' + qtdCondi + '"> </tr></table>');
    $('#condTD' + qtdCondi + '').append('<td class="m-1"><input type="text" name="condLine[]" class="form-control" value="' + line + '" ></td>');
    $('#condTD' + qtdCondi + '').append('<td class="m-1"><input type="number" name="condP_start[]" class="form-control" value="' + p_start + '" ></td>');
    $('#condTD' + qtdCondi + '').append('<td class="m-1"><input type="hidden" name="condTypeVal[]" class="form-control" value="' + typeVal + '" ><input type="text" class="form-control" readonly value="' + typeText + '" ></td>');
    $('#condTD' + qtdCondi + '').append('<td class="m-1"><input type="hidden" class="form-control" name="condValor[]" readonly value="' + valorId + '" ><input type="text" class="form-control" readonly value="' + valorNm + '" ></td>');
    $('#condTD' + qtdCondi + '').append('<td class="m-1"><button type="button" class="btn btn-primary m-1" nm="remCond" id="remCond" data-qtd="' + qtdCondi + '">Remover</button></td>');
    $('#condAdicionada').append('</div>');


}


////Graficos

function showModal(sensor) {

    // alert('Executado');
    $('.modal').css({opacity: 0, display: 'flex'}).animate({
        opacity: 1
    }, 300);
    $('body').addClass('overflow');

    $('#nmclasse').val(sensor);
    $('#btn-dia').addClass('active');
    $('.container-grafico').html("<img src='images/carregando.gif' >");
    carregaGrafico('dia');
}


function carregaGrafico(periodo, idItem) {
    //remove as classes active
    // $('div.btn').removeClass('active');
    // $('#btn-'+periodo).addClass('active');

    $('.container-grafico').html("");
    // var sensor =  $('#nmclasse').val();
    $('.container-grafico').html("<img src='" + $url_atual + "images/carregando.gif' >");
    $('.container-grafico').load($url_atual + 'modal/menu/graficosGerar/' + periodo + '/' + idItem + '.html');
}


function ativaBotaoSalvar() {
    $('.btn-primary').removeAttr("disabled");
    //alert('Executado');
}

function removeEspaco(str, id) {
    //alert("Oi");

    str = $("#" + str).val();

    var find = ["ã", "à", "á", "ä", "â", "è", "é", "ë", "ê", "ì", "í", "ï", "î", "ò", "ó", "ö", "ô", "ù", "ú", "ü", "û", "ñ", "ç"];
    "à", "á", "ä", "â", "è", "é", "ë", "ê", "ì", "í", "ï", "î", "ò", "ó", "ö", "ô", "ù", "ú", "ü", "û", "ñ", "ç";
    var replace = ["a", "a", "a", "a", "a", "e", "e", "e", "e", "i", "i", "i", "i", "o", "o", "o", "o", "u", "u", "u", "u", "n", "c"];

    for (var i = 0; i < find.length; i++) {
        str = str.replace(new RegExp(find[i], 'gi'), replace[i]);
    }

    var desired = str.replace(/\s+/g, '-');
    desired = desired.toLowerCase();

    $("#" + id).val(desired);
}


///CKEDITOR

function  ckeditorIniciar() {
    //CKEDITOR.extraPlugins = 'youtube';
    CKEDITOR.addCss('.cke_editable { font-size: 15px; padding: 2em; }');
    CKEDITOR.replace('conteudo2', {

        extraAllowedContent: 'h3{clear};h2{line-height};h2 h3{margin-left,margin-top}',
        extraPlugins: 'print,format,font,colorbutton,justify,uploadimage',

        // Upload images to a CKFinder connector (note that the response type is set to JSON).
        uploadUrl: $url_atual + 'post/Ckeditor/Listarimgsss.html',

        // Configure your file manager integration. This example uses CKFinder 3 for PHP.
        filebrowserBrowseUrl: $url_atual + 'admin/plugins/ckfinder/ckfinder.html?type=Files',
        filebrowserImageBrowseUrl: $url_atual + 'admin/plugins/ckfinder/ckfinder.html?type=Images',
        filebrowserUploadUrl: $url_atual + 'admin/post/Ckeditor/Enviar_arquivos.html',
        filebrowserImageUploadUrl: $url_atual + 'admin/post/Ckeditor/Enviar_img.html',

        height: 560,
        // The following options are not necessary and are used here for presentation purposes only.
        // They configure the Styles drop-down list and widgets to use classes.

        stylesSet: [{
                name: 'Narrow image',
                type: 'widget',
                widget: 'image',
                attributes: {
                    'class': 'image-narrow'
                }
            },
            {
                name: 'Wide image',
                type: 'widget',
                widget: 'image',
                attributes: {
                    'class': 'image-wide'
                }
            }
        ],

        // Load the default contents.css file plus customizations for this sample.
        contentsCss: [
            'http://cdn.ckeditor.com/4.14.0/full-all/contents.css',
            'assets/css/widgetstyles.css'
        ],

        // Configure the Enhanced Image plugin to use classes instead of styles and to disable the
        // resizer (because image size is controlled by widget styles or the image takes maximum
        // 100% of the editor width).
        image2_alignClasses: ['image-align-left', 'image-align-center', 'image-align-right'],
        image2_disableResizer: true,
        removeDialogTabs: 'image:advanced;link:advanced'
    });

}