
<?php
include_once './config/config.php';
?>
<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
        <meta name="author" content="Creative Tim">
        <title>Imaral Pneus</title>
        <!-- icon -->
        <link rel="icon" href="<?= HOST ?>/assets/img/brand/logosite.png" type="image/ico">
        <!-- Fontes -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
        <!-- Icones -->
        <link href="<?= HOST ?>/assets/vendor/alertifyjs/alertify.min.css" rel="stylesheet" type="text/css"/>

        <link rel="stylesheet" href="<?= HOST ?>/assets/vendor/nucleo/css/nucleo.css" type="text/css">
        <script src="<?= HOST ?>/assets/vendor/alertifyjs/alertify.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="<?= HOST ?>/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
       
          <!-- Page plugins -->
        <link rel="stylesheet" href="<?= HOST ?>/assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="<?= HOST ?>/assets/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">
        <link rel="stylesheet" href="<?= HOST ?>/assets/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css">
        <!-- Argon CSS -->
        <link rel="stylesheet" href="<?= HOST ?>/assets/css/argon.css?v=1.2.0" type="text/css">
        
          <script src="<?= HOST ?>/assets/js/jquery.mask.min" type="text/javascript"></script>
          <!-- Optional JS -->

  
   
        
    </head>

    <body>
        <?php
        //menu lateral
        include_once './pages/menu_lateral.php';
        ?>
        <!-- tela principal -->
        <div class="main-content" id="panel">
            <?php
            //menu topo
            include_once './pages/menu_topo.php';
            ?>
            <!-- Painel -->
            <div id="conteudo">
                <?php
                new Loader($_GET);
                include 'pages/modals.php';
                ?>  
            </div>




            <!-- Footer -->
            <footer class="footer pt-0">
                <div class="row align-items-center justify-content-lg-between">
                    <div class="col-lg-6">
                        <div class="copyright text-center  text-lg-left  text-">
                            &copy; 2020 <a href="#" class="font-weight-bold ml-1 text-info" target="_self">Imaral Pneus</a>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <ul class="nav nav-footer justify-content-center justify-content-lg-end">
                            <li class="nav-item">
                                <a href="#" class="nav-link" target="_self"></a>
                            </li>
                                                       
                        </ul>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <!-- Argon Scripts -->
    <!-- Core -->
    <?php
    ?>
    <script src="<?= HOST ?>/assets/js/jquery.mask.min" type="text/javascript"></script>
    <script src="<?= HOST ?>/assets/vendor/jquery/dist/jquery.min.js"></script>
    <script src="<?= HOST ?>/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="<?= HOST ?>/assets/vendor/js-cookie/js.cookie.js"></script>
    <script src="<?= HOST ?>/assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
    <script src="<?= HOST ?>/assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
    <!-- Optional JS -->
    <script src="<?= HOST ?>/assets/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="<?= HOST ?>/assets/vendor/chart.js/dist/Chart.extension.js"></script>
     <script src="<?= HOST ?>/assets/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?= HOST ?>/assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?= HOST ?>/assets/vendor/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?= HOST ?>/assets/vendor/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="<?= HOST ?>/assets/vendor/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?= HOST ?>/assets/vendor/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?= HOST ?>/assets/vendor/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?= HOST ?>/assets/vendor/datatables.net-select/js/dataTables.select.min.js"></script>
    <!-- Argon JS -->
    <script src="<?= HOST ?>/assets/js/argon.js?v=1.2.0"></script>
    <script src="<?= HOST ?>/assets/vendor/jquery/jquery.form.min.js" type="text/javascript"></script>
    <script src="<?= HOST ?>/assets/js/funcao.js" type="text/javascript"></script>
</body>

</html>
