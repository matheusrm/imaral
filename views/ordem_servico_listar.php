


<div class="header ">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-black d-inline-block mb-0"> <?= $dados['titulo']; ?></h6>
                </div>
                <div class="col-lg-6 col-5 text-right">
                    <a href="#"><i class="btn  btn-neutral text-info abrirModal " data-toggle="modal" data-target=".modal-cadastros"  data-title="Novo <?= $dados['titulo']; ?>"  data-type="<?= $dados['nm_class_mani']; ?>/acao/novo.html">Novo</i></a>             
                </div>
            </div>
            <div class="table-responsive">

                <div>
                    <table class="table table-flush " id="datatable-buttons"  style="width:100%">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col" class="sort" data-sort="name">ID</th>
                                <th scope="col" class="sort" data-sort="budget">Cliente</th>
                                <th scope="col" class="sort" data-sort="budget">Veiculo</th>
                                <th scope="col" class="sort" data-sort="name">KM</th>
                                <th scope="col" class="sort" data-sort="budget">Observação</th>
                                <th scope="col" class="sort" data-sort="budget">Estado</th>
                                <th scope="col" class="sort" data-sort="budget">Tecnico</th>
                                <th scope="col" class="sort" data-sort="budget">Placa</th>
                                <th scope="col" class="sort" data-sort="budget">Valor</th>


                                <th scope="col"></th>

                            </tr>
                        </thead>
                        <tbody class="list">

                            <?php
                            foreach ($dados['listagem'] as $u) {
                                ?>
                                <tr>
                                    <td><?= $u['idOrdem'] ?></td>   
                                    <td><?= $u['clienteid'] ?></td>
                                    <td><?= $u['equipamentoid'] ?></td>
                                    <td><?= $u['km'] ?></td> 
                                    <td><?= $u['observacao'] ?></td> 
                                    <td>
                                        <?php
                                        if ($u['estado'] == 'Aguardo') {
                                            ?>
                                            <i class="fas fa-exclamation-circle fa-2x text-warning"></i>                                            
                                            <?php
                                        } elseif ($u['estado'] == 'Remarcado') {
                                            ?>
                                            <i class="fas fa-history fa-2x text-yellow"></i>
                                            <?php
                                        } else {
                                            ?>
                                            <i class="far fa-check-circle fa-2x text-success"></i>
                                            <?php
                                        }
                                        ?>

                                    </td> 
                                    <td>
                                        <?php
                                        if ($u['funcionarioid'] == '1') {
                                            ?>
                                            <i>
                                                <h3> Matheus</h3>
                                            </i>                                            
                                            <?php
                                        } elseif ($u['funcionarioid'] == '2') {
                                            ?>
                                            <h3> Peba</h3>
                                            <?php
                                        } elseif ($u['funcionarioid'] == '200') {
                                            ?>
                                            <h3> Wesley</h3>     
                                            <?php
                                        } elseif ($u['funcionarioid'] == '201') {
                                            ?>
                                            <h3> Vitor</h3>     
                                            <?php
                                        } elseif ($u['funcionarioid'] == '202') {
                                            ?>
                                            <h3> Cleitão</h3>     
                                            <?php
                                        } elseif ($u['funcionarioid'] == '203') {
                                            ?>
                                            <h3> Flavio</h3>     
                                            <?php
                                        } else {
                                            ?>
                                            <h3> Bruno</h3>     
                                            <?php
                                        }
                                        ?>

                                    </td>
                                    <td><?= $u['placa'] ?></td> 
                                    <td><?= $u['valorTotal'] ?></td> 




                                    <td class="text-right">
                                        <div class="dropdown">
                                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                <a href="#" title="Editar" class="dropdown-item abrirModal" data-toggle="modal" data-target=".modal-cadastros" data-title="Editar registro."  data-type="<?= $dados['nm_class_mani']; ?>/acao/editar/<?= $u['idOrdem'] ?>.html">Editar</a>                                                
                                                <a href="#" title="Excluir" class="dropdown-item abrirModalConfirmacao" data-toggle="modal" data-target=".modal-confirmacao" data-title="Exclusao de Registro" data-type="<?= $dados['nm_class_mani']; ?>/acao/excluir/<?= $u['idOrdem'] ?>/.html">Excluir</a>

                                            </div>
                                        </div>
                                    </td>

                                </tr> 
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
