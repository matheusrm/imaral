<?php
//var_dump($dados);
//$cropper->flush();


if (isset($dados['acao']) && $dados['acao'] == 'editar' || $dados['acao'] == 'excluir') {
    $registros = $dados['dados'];
    foreach ($registros as $value) {
        ?>
        <script>
            $(document).ready(function () {

                $("#acao").val("<?= $dados['acao']; ?>");
                $("#idRegistro").val("<?= $value['idOrdem']; ?>");
                if ("<?= $dados['acao']; ?>" == "editar") {
        <?php
        foreach ($value as $key => $valor) {
            if ($key == "ativo" && $valor == 0) {
                echo "$('#$key').attr('checked', false);";
            } else {
                echo "$('#$key').val('$valor');";
            }
        }
        ?>

                } else {
                    $(".formulario").html("Confirma a exclusão do registro?");
                }
            });
        </script>
        <?php
    }
}
?>
<head>


</head> 
<div class="container-fluid">
    <div class="row ">
        <div class="card-body col-xl-14">
            <form id="formulario"  enctype="multipart/form-data">
                <input value="<?= HOST ?>/post/<?= $dados['nm_class_mani']; ?>/gravar.html" id="url" type="hidden">
                <input value="novo" id="acao" name="acao" type="hidden">
                <input value="0" id="idRegistro" name="idRegistro" type="hidden">
                <span class="formulario">
                    <h6 class="heading-small text-muted mb-4">Dados da ordem de serviço</h6>
                    <div class="pl-lg-4">
                        <div class="row">
                            <div class="col-lg-6">
                                <?php
                               // var_dump($dados['listarlista']);
                                
                                ?>

     
    
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Cliente</label>
                                    <select class="form-control" id="exampleFormControlSelect1">
                                        <?php
                                         foreach ($dados['listarlista'] as $u) {
                                        ?>
                                        
                                        <option value="<?php echo $u['idCliente']; ?>"><?php echo $u['nomeCliente']; ?> </option>
                                        <?php
                                         }
                                         ?>
                                    </select>
                                </div>
    
                            </div> 
                       <div class="col-lg-6">
                       
                            <div class="form-group">
                                    <label for="exampleFormControlSelect1">veiculo</label>
                                    <select class="form-control" id="exampleFormControlSelect1">
                                        <?php
                                         foreach ($dados['listarcarro'] as $u) {
                                        ?>
                                        
                                        <option value="<?php echo $u['idEquipamento']; ?>"><?php echo $u['carro']; ?> </option>
                                        <?php
                                         }
                                         ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-username">kilometragem do veículo</label>
                                    <input type="text" name='km' id="km" class="form-control" placeholder="KM">
                                </div>
                            </div> 
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-username">Observação do serviço</label>
                                    <input type="text" name='observacao' id="observacao" class="form-control" placeholder="observações">
                                </div>
                            </div> 
                             <div class="col-lg-6">
                       
                            <div class="form-group">
                                    <label for="exampleFormControlSelect1">Mecânico</label>
                                    <select class="form-control" id="exampleFormControlSelect1">
                                        <?php
                                         foreach ($dados['listarmecanico'] as $u) {
                                        ?>
                                        
                                        <option value="<?php echo $u['idFuncionario']; ?>"><?php echo $u['nomeFuncionario']; ?> </option>
                                        <?php
                                         }
                                         ?>
                                    </select>
                                </div>
                            </div>
            
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-username">Placa do Veículo:</label>
                                    <input type="text" name='placa' id="placa" class="form-control" placeholder="Placa">
                                </div>
                            </div> 
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-username">Valor Total:</label>
                                    <input type="text" name='valorTotal' id="valorTotal" class="form-control" placeholder="Total">
                                </div>
                            </div>
                        </div> 
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-control-label" for="input-username">Estado do serviço</label>
                                <select class="form-control" id="estado" name="estado">
                                    <option>Finalizado</option>
                                    <option>Aguardo</option>
                                    <option>Remarcado</option>                
                                </select>

                            </div>
                        </div> 
                    </div>
                </span>
            </form>
        </div> 
    </div>                            
</div>                            



