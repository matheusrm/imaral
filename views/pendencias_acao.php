<?php
//var_dump($dados);
//$cropper->flush();


if (isset($dados['acao']) && $dados['acao'] == 'editar' || $dados['acao'] == 'excluir') {
    $registros = $dados['dados'];
    foreach ($registros as $value) {
        ?>
        <script>
            $(document).ready(function () {

                $("#acao").val("<?= $dados['acao']; ?>");
                $("#idRegistro").val("<?= $value['idPendencia']; ?>");
                if ("<?= $dados['acao']; ?>" == "editar") {
        <?php
        foreach ($value as $key => $valor) {
            if ($key == "ativo" && $valor == 0) {
                echo "$('#$key').attr('checked', false);";
            } else {
                echo "$('#$key').val('$valor');";
            }
        }
        ?>

                } else {
                    $(".formulario").html("Confirma a exclusão do registro?");
                }
            });
        </script>
        <?php
    }
}
?>
<head>
    <script src="<?= HOST ?>../assets/js/jquery.mask.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function () {
        $("#dataPendencia").mask("00/00/0000")

    })

    </script>
</head> 
<div class="container-fluid">
    <div class="row ">
        <div class="card-body col-xl-14">
            <form id="formulario"  enctype="multipart/form-data">
                <input value="<?= HOST ?>/post/<?= $dados['nm_class_mani']; ?>/gravar.html" id="url" type="hidden">
                <input value="novo" id="acao" name="acao" type="hidden">
                <input value="0" id="idRegistro" name="idRegistro" type="hidden">
                <span class="formulario">
                    <h6 class="heading-small text-muted mb-4">Dados do cliente</h6>
                    <div class="pl-lg-4">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-username">Descrição</label>
                                    <input type="text" name='descPendencia' id="descPendencia" class="form-control" placeholder="Descrição">
                                </div>
                            </div> 
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-username">Observações</label>
                                    <input type="text" name='obsPendencia' id="obsPendencia" class="form-control" placeholder="Observações">
                                </div>
                            </div> 
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-username">Data de Termino</label>
                                    <input type="text" name='dataPendencia' id="dataPendencia" class="form-control" placeholder="Data">
                                </div>
                            </div> 

                        </div>
                </span>
            </form>
        </div>
                    
    </div>                            
</div>                            
</div>                            



