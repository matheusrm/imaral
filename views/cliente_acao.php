<?php
//var_dump($dados);

//$cropper->flush();


if (isset($dados['acao']) && $dados['acao'] == 'editar' || $dados['acao'] == 'excluir') {
    $registros = $dados['dados'];
    foreach ($registros as $value) {
        
        ?>
        <script>
            $(document).ready(function () {

                $("#acao").val("<?= $dados['acao']; ?>");
                $("#idRegistro").val("<?= $value['idCliente']; ?>");
                if ("<?= $dados['acao']; ?>" == "editar") {
        <?php
        foreach ($value as $key => $valor) {
            if ($key == "ativo" && $valor == 0) {
                echo "$('#$key').attr('checked', false);";
            } else {
                echo "$('#$key').val('$valor');";
            }
        }
        ?>

                } else {
                    $(".formulario").html("Confirma a exclusão do registro?");
                }
            });
        </script>
        <?php
    }
}
?>
                <head>

    <script src="<?= HOST ?>../assets/js/jquery.mask.min.js" type="text/javascript"></script>
    <script type="text/javascript">
   $(document).ready(function(){
       $("#telefoneCliente").mask("(00) 00000-0000")
       $("#cepCliente").mask("00.000-000")
       
   })
  
    </script>
</head> 
<div class="container-fluid">
    <div class="row ">
        <div class="card-body col-xl-14">
            <form id="formulario"  enctype="multipart/form-data">
                <input value="<?= HOST ?>/post/<?= $dados['nm_class_mani']; ?>/gravar.html" id="url" type="hidden">
                <input value="novo" id="acao" name="acao" type="hidden">
                <input value="0" id="idRegistro" name="idRegistro" type="hidden">
                <span class="formulario">
                    <h6 class="heading-small text-muted mb-4">Dados do cliente</h6>
                    <div class="pl-lg-4">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-username">Nome</label>
                                    <input type="text" name='nomeCliente' id="nomeCliente" class="form-control" placeholder="Nome completo">
                                </div>
                            </div> 
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-username">Gênero</label>
                                    <input type="text" name='generoCliente' id="generoCliente" class="form-control" placeholder="Gênero">
                                </div>
                            </div> 
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-username">Telefone</label>
                                    <input type="text" name='telefoneCliente' id="telefoneCliente" class="form-control" placeholder="Telefone">
                                </div>
                            </div> 
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-username">Endereço</label>
                                    <input type="text" name='enderecoCliente' id="enderecoCliente" class="form-control" placeholder="Endereço">
                                </div>
                            </div>
                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label class="form-control-label" for="input-username">CEP</label>
                                    <input type="text" name='cepCliente' id="cepCliente" class="form-control " placeholder="Ex.: 00000-000">
                                </div>
                            </div> 
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-username">Estado civil</label>
                                    <input type="text" name='civilCliente' id="civilCliente" class="form-control" placeholder="Estado civil">
                                </div>
                            </div> 
                        </div>

                        <div class="col-lg-14">
                            <div class="form-group">
                                <label class="form-control-label" for="email">Email</label>
                                <input type="email" name='emailCliente'  id="emailCliente" class="form-control" placeholder="email">
                            </div>
                        </div>






                    </div>
                </span>
            </form>
        </div> 
    </div>                            
</div>                            



