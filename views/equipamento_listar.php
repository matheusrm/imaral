


<div class="header ">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-black d-inline-block mb-0"> <?= $dados['titulo']; ?></h6>
                </div>
                <div class="col-lg-6 col-5 text-right">
                    <a href="#"><i class="btn  btn-neutral text-info abrirModal " data-toggle="modal" data-target=".modal-cadastros"  data-title="Novo <?= $dados['titulo']; ?>"  data-type="<?= $dados['nm_class_mani']; ?>/acao/novo.html">Novo</i></a>             
                </div>
            </div>
            <div class="table-responsive">

                <div>
                    <table class="table table-flush " id="datatable-buttons"  style="width:100%">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col" class="sort" data-sort="name">ID</th>
                                <th scope="col" class="sort" data-sort="budget">Veículo</th>
                                <th scope="col" class="sort" data-sort="budget">Marca</th>
                                
                             

                                <th scope="col"></th>

                            </tr>
                        </thead>
                        <tbody class="list">

                            <?php
                            foreach ($dados['listagem'] as $u) {
                                ?>
                                <tr>
                                    <td><?= $u['idEquipamento'] ?></td>   
                                    <td><?= $u['carro'] ?></td>
                                    <td><?= $u['marca'] ?></td>
                                    



                                    <td class="text-right">
                                        <div class="dropdown">
                                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                <a href="#" title="Editar" class="dropdown-item abrirModal" data-toggle="modal" data-target=".modal-cadastros" data-title="Editar registro."  data-type="<?= $dados['nm_class_mani']; ?>/acao/editar/<?= $u['idEquipamento'] ?>.html">Editar</a>                                                
                                                <a href="#" title="Excluir" class="dropdown-item abrirModalConfirmacao" data-toggle="modal" data-target=".modal-confirmacao" data-title="Exclusao de Registro" data-type="<?= $dados['nm_class_mani']; ?>/acao/excluir/<?= $u['idEquipamento'] ?>/.html">Excluir</a>

                                            </div>
                                        </div>
                                    </td>

                                </tr> 
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
