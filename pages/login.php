<?php
//include 'config/config2.php';
?>

<!DOCTYPE html>
<html lang="pt">
    <head>
        <title>Login</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="<?=HOST?>/assets/vendor/alertifyjs/alertify.min.css" rel="stylesheet" type="text/css"/>
        <script src="<?=HOST?>/assets/vendor/alertifyjs/alertify.min.js" type="text/javascript"></script>
        <!--===============================================================================================-->	
        <link rel="icon" type="image/png" href="<?=HOST?>/assets/images/icons/favicon.ico"/>
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="<?=HOST?>/assets/vendor/bootstrap/css/bootstrap.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="<?=HOST?>/assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="<?=HOST?>/assets/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="<?=HOST?>/assets/vendor/animate/animate.css">
        <!--===============================================================================================-->	
        <link rel="stylesheet" type="text/css" href="<?=HOST?>/assets/vendor/css-hamburgers/hamburgers.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="<?=HOST?>/assets/vendor/animsition/css/animsition.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="<?=HOST?>/assets/vendor/select2/select2.min.css">
        <!--===============================================================================================-->	
        <link rel="stylesheet" type="text/css" href="<?=HOST?>/assets/vendor/daterangepicker/daterangepicker.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="<?=HOST?>/assets/css/util.css">
        <link rel="stylesheet" type="text/css" href="<?=HOST?>/assets/css/main.css">
        <!--===============================================================================================-->
    </head>
    <body>

        <div class="limiter">
            <div class="container-login100"  >
        
                <div class="wrap-login100 p-t-50 p-b-90">
                
                    <div id="conteudo">
                        <form class="login100-form validate-form flex-sb flex-w"  enctype="multipart/form-data" id="formulario">

                            <input type="hidden" name="url" id="url" value="<?=HOST?>/post/usuario/logar.html">

                            <span class="login100-form-title p-b-51">
                                Login
                            </span>


                            <div class="wrap-input100 validate-input m-b-16" >
                                <input class="input100" type="text" name="email" placeholder="Email">
                                <span class="focus-input100"></span>
                            </div>


                            <div class="wrap-input100 validate-input m-b-16" data-validate = "Password is required">
                                <input class="input100" type="password" name="senha" placeholder="Senha">
                                <span class="focus-input100"></span>
                            </div>

                            <div class="flex-sb-m w-full p-t-3 p-b-24">
                                <div class="contact100-form-checkbox">
                                    <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
                                    <label class="label-checkbox100" for="ckb1">
                                        Lembrar
                                    </label>
                                </div>

                                <div>
                                    <a href="#" class="txt1">
                                        Esqueceu sua senha?
                                    </a>
                                </div>
                            </div>

                            <div class="container-login100-form-btn m-t-17">
                                <button type="button" class="login100-form-btn " id="enviaForm">
                                    Login
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>


        <div id="dropDownSelect1"></div>

        <!--===============================================================================================-->
        <script src="<?=HOST?>/assets/vendor/jquery/jquery-3.2.1.min.js"></script>
        <!--===============================================================================================-->
        <script src="<?=HOST?>/assets/vendor/animsition/js/animsition.min.js"></script>
        <!--===============================================================================================-->
        <script src="<?=HOST?>/assets/vendor/bootstrap/js/popper.js"></script>
        <script src="<?=HOST?>/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
        <!--===============================================================================================-->
        <script src="<?=HOST?>/assets/vendor/select2/select2.min.js"></script>
        <!--===============================================================================================-->
        <script src="<?=HOST?>/assets/vendor/daterangepicker/moment.min.js"></script>
        <script src="<?=HOST?>/assets/vendor/daterangepicker/daterangepicker.js"></script>
        <!--===============================================================================================-->
        <script src="<?=HOST?>/assets/vendor/countdowntime/countdowntime.js"></script>
        <!--===============================================================================================-->
        <script src="<?=HOST?>/assets/js/main.js"></script>
        <script src="<?=HOST?>/assets/vendor/jquery/jquery.form.min.js" type="text/javascript"></script>
     
      
        <script src="<?=HOST?>/assets/js/funcao.js" type="text/javascript"></script>
        

    </body>
</html>