
<div class="envioAjax modal fade modal-cadastros" id="modalFormulario" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="titleModal"></h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" id="conteudoModal">
                <h4></h4>
            </div>
            <div class="modal-footer ">

                <div class="col-md-4 col-sm-4 ">

                    <div class="progress">
                        <div id="barraprogresso" class="progress-bar progress-bar-striped bg-success" role="progressbar"  aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
                <span id="carregamento"></span>
                <button type="button" class="btn btn-secondary " data-dismiss="modal" id="fecharModal">Fechar</button>
                <button type="button" class="btn btn-primary"   id="enviaForm">Salvar</button>
            </div>
        </div>
    </div>
</div>

<div class="envioAjax modal fade modal-confirmacao" id="modalConfirmacao" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="titleModal"</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" id="conteudoModal">
                <h4></h4>
            </div>
            <div class="modal-footer">
                <span id="carregamento"></span>
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="fecharModal">Não</button>
                <button type="button" class="btn btn-primary" id="enviaForm">Sim</button>
            </div>
        </div>
    </div>
</div>


<div class="envioAjax modal fade modal-graficos" id="ModalGrafico" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="titleModal"></h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" id="conteudoModal">
                <h4></h4>
            </div>
            <div class="modal-footer">
                <span id="carregamento"></span>
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="fecharModal">Fechar</button>
            </div>
        </div>
    </div>
</div>



<div class="envioAjax modal fade modal-ckeditor" id="modalFormulario" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="titleModal"></h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" id="conteudoModal">
                <h4></h4>
            </div>
            <div class="modal-footer ">

                <div class="col-md-4 col-sm-4 ">

                    <div class="progress">
                        <div id="barraprogresso" class="progress-bar progress-bar-striped bg-success" role="progressbar"  aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
                <span id="carregamento"></span>
                <button type="button" class="btn btn-secondary " data-dismiss="modal" id="fecharModal">Fechar</button>
                <button type="button" class="btn btn-primary"   id="enviaCkeditor">Salvar</button>
            </div>
        </div>
    </div>
</div>
