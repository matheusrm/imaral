<!-- Sidenav -->
<nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <!-- Brand -->
        <div class="sidenav-header d-flex align-items-center">
            <a class="navbar-brand" href="javascript:void(0)">Imaral Pneus
                <img src="<?= HOST ?>/assets/img/brand/logoicon.jpeg" class="navbar-brand-img" alt="...">
            </a>
            <div class="ml-auto">
                <!-- Sidenav toggler -->
                <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
                    <div class="sidenav-toggler-inner">
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar-inner">
            <!-- menus -->
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <!-- Nav items -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link active" href="<?=HOST?>/home/index.html">
                            <i class="ni ni-tv-2 text-primary"></i>
                            <span class="nav-link-text">Home</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="<?=HOST?>/ordem_servico/index.html">
                            <i class="ni ni-tv-2 fas fa-tools"></i>
                            <span class="nav-link-text">Ordem de serviço</span>
                        </a>
                    </li>
                    <li class="nav-item">
                       <a class="nav-link" href="<?=HOST?>/agendamentos/index.html">
                            <i class="ni ni-tv-2 far fa-clipboard"></i>
                            <span class="nav-link-text">Agendamentos</span>
                        </a>
                    </li>                            
                    <li class="nav-item">
                        <a class="nav-link" href="<?=HOST?>/pendencias/index.html">
                            <i class="ni ni-single-02 text-yellow"></i>
                            <span class="nav-link-text">Pendências</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <i class="ni ni-bullet-list-67 text-default"></i>
                            <span class="nav-link-text">Relatórios</span>
                        </a>
                    </li>
                   
                    <li class="nav-item">
                        <a class="nav-link" href="<?=HOST?>/usuario/index.html">
                            <i class="ni ni-circle-08 text-pink"></i>
                            <span class="nav-link-text">Configurações de usuario</span>
                        </a>
                    </li>
                      <li class="nav-item">
                        <a class="nav-link" href="<?=HOST?>/cliente/index.html">
                            <i class="ni ni-key-25 text-info"></i>
                            <span class="nav-link-text">Cadastro de cliente</span>
                        </a>
                    </li>
                     <li class="nav-item">
                       <a class="nav-link" href="<?=HOST?>/equipamento/index.html">
                            <i class="ni ni-key-25 fas fa-car"></i>
                            <span class="nav-link-text">Cadastro de veículo</span>
                        </a>
                    </li>
                     <li class="nav-item">
                      <a class="nav-link" href="<?=HOST?>/funcionario/index.html">
                            <i class="ni ni-key-25 far fa-address-card"></i>
                            <span class="nav-link-text">Cadastro de funcionário</span>
                        </a>
                    </li>
                </ul>

            </div>
        </div>
    </div>
</nav>
