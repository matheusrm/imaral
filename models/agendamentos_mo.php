<?php

class agendamentos_mo {

    protected $Logado = false;
    protected $conexao;
    protected static $adm = false;
    protected $tbl = "agendamentos";
    private $mensagens;
    private $select;

    public function __construct() {
        $this->mensagens = new Mensagens();
        $this->select = new Select();
    }

    public function Listar($id = null, $ativo = null) {
        $condicao = array();

        if ($id <> null) {
            $condicao = ['idAgendamentos', '=', $id];
        }

        if ($ativo == "S") {
            count($condicao) > 0 ? array_push($condicao, 'AND', 'ativo', '=', '1') : array_push($condicao, 'ativo', '=', '1');
        }

        $resultado = $this->select->SelectP(NM_COL[$this->tbl], $this->tbl, $condicao, "order by idAgendamentos asc");
        return $resultado;
    }

    public function Listar_cliente($id = null, $ativo = null) {
        $condicao = array();
        $sql = "SELECT * FROM agendamentos;";

        $resultado = $this->select->SelectQuery($sql);
        return $resultado;
    }

    public function Gravar($d = null, $img = null) {
        try {
         //   var_dump($d);
            $s = new Select();
            $error = array();

            // var_dump($d);
            //Verifica se a acao é para excluir
            if ($d['acao'] == 'excluir') {
                $e = new Delete();
                $cone = new Database();
                $trans = $cone->Begin();
                if ($e->Deletar($trans, $this->tbl, ['idAgendamentos', '=', $d['idRegistro']])) {
                    array_push($error, $this->mensagens->Msg("exSucesso"));
                    $cone->Commit();
                } else {
                    array_push($error, $this->mensagens->Msg("regNPSE"));
                    $cone->Rollback();
                }
                echo json_encode($error);
                exit();
            }


            ///----- Validação ------
            if (empty($d['ClienteAgendamentos']) && $d['ClienteAgendamentos'] == "") {
                array_push($error, $this->mensagens->Msg("nmVazio"));
            }
            if (empty($d['dataAgendamento']) && $d['dataAgendamento'] == "") {
                array_push($error, $this->mensagens->Msg("emailVazio"));
            }


            /* if (isset($d['classeacessoid']) && $d['classeacessoid'] < 0) {
              array_push($error, $this->mensagens->Msg("SelclasseDeacesso"));
              } */

            $d['acao'] == "editar" ? $cond = ['ClienteAgendamentos', '=', $d['ClienteAgendamentos'], "AND", 'idAgendamentos', '<>', $d['idRegistro']] : $cond = ['ClienteAgendamentos', '=', $d['ClienteAgendamentos']];


           // $s->Count($this->tbl, $cond) <> 0 ? array_push($error, $this->mensagens->Msg("emailExiste")) : 0;


            //exit();
            // echo $s->Count($this->tbl, $cond);
            // 
            // 
            // Upload da imagem do usuario
            ///Verificação das imgs
            // sleep(2);
            if (count($error) <> 0) {
                echo json_encode($error);
                exit();
            }
            //exit();
            ///Remove as colunas do array
            $acao = $d['acao'];
            $idRegistro = $d['idRegistro'];
            unset($d['acao']);
            unset($d['idRegistro']);
            unset($d['confirmar']);
          //  unset($d['dataAgendamento']);
        //    echo '<br><br>';
           // var_dump($d);

            $nm_colunas = array_keys($d);
            $nm_valores = array_values($d);
          //  var_dump($nm_valores);
            //var_dump($nm_colunas); 
             //echo '<br><br>';
            ////Inicia a conexão com o banco de dados
            $cone = new Database();
            $trans = $cone->Begin();

            if ($acao == "editar") {
                $u = new Update();
                $condicao = ['idAgendamentos', '=', $idRegistro];
                if ($u->Atualizar($trans, $nm_colunas, $nm_valores, $this->tbl, $condicao)) {
                    array_push($error, $this->mensagens->Msg('atSucesso'));
                    $cone->Commit();
                } else {
                    array_push($error, $this->mensagens->Msg('erroAtualizar'));
                    $cone->Rollback();
                }
            } if ($acao == "novo") {
                $u = new Insert();
                if ($u->Insere($trans, $nm_colunas, $nm_valores, $this->tbl)) {
                    array_push($error, $this->mensagens->Msg('inSucesso'));
                    $cone->Commit();
                } else {
                    array_push($error, $this->mensagens->Msg('inErro'));
                    $cone->Rollback();
                }
            }

            echo json_encode($error);
        } catch (Exception $exc) {
            // var_dump($exc);
            array_push($error, $this->mensagens->Msg('inErro'));
            //  echo json_encode($error);
            exit();
        }
    }

}

?>
