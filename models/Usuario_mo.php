<?php

class Usuario_mo {

    protected $Logado = false;
    protected $conexao;
    protected static $adm = false;
    protected $tbl = "usuario";
    private $mensagens;

    public function __construct() {
        $this->mensagens = new Mensagens();
    }

    public function Listar($id = null) {
        $s = new Select();
        $condicao = null;
        if ($id <> null) {
            $condicao = ['id', '=', $id];
        }

        $usuario = $s->SelectP(NM_COL['usuario'], $this->tbl, $condicao);
        return $usuario;
    }

    public function Sair() {
        session_destroy();
        header("Location: " . HOST . "/login.html");
    }

    public function Logar($dados) {
        $data = new Database();
        //$data = 
        $this->conexao = $data->conexao();
        //var_dump($dados);
        $query = "SELECT * FROM usuario as u  WHERE u.email = ? AND u.senha = ? ";
        $stmt = $this->conexao->prepare($query);

        $stmt->bindValue(1, $dados['email']);
        $stmt->bindValue(2, md5($dados['senha']));
        $retorno = array();
        if ($stmt->execute()) {
            if ($stmt->rowCount() == 1) {

                $this->Logado = true;
                $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach ($results as $row) {
                    if (!$row['ativo']) {
                        array_push($retorno, $this->mensagens->msg("userInativo"));
                        echo json_encode($retorno);
                        exit();
                    } else {
                        $_SESSION['itIdUser'] = $row['id'];
                        $_SESSION['itNmUser'] = $row['nome'];
                        $row['imagem'] == NULL ? $_SESSION['itImgUser'] = "assets/img/theme/user.png" : $_SESSION['itImgUser'] = $row['imagem'];
                        $_SESSION['itEmailUser'] = $row['email']; // email
                        $_SESSION['itIdCa'] = $row['nome']; // classe acesso
                        ///Listas os que não pode acessar
                        array_push($retorno, $this->mensagens->msg("logado"));

                        echo json_encode($retorno);
                        exit();
                    }
                }
                //header("Location: ".HOST."/index.html");
                //return $this->getLogado();
            } else {
                //$this->Logado = false;
                array_push($retorno, $this->mensagens->msg("erroLoginSenha"));
                echo json_encode($retorno);

                //echo  $this->getLogado();
            }
        } else {
            array_push($retorno, $this->mensagens->msg("inErro"));
            echo json_encode($retorno);
            exit();
        }
    }

    public function getAdm() {
        return $this::$adm;
    }

    public function getLogado() {

        //return $this->Logado;
        if (isset($_SESSION['itIdUser']) && isset($_SESSION['itNmUser'])) {
            $this->Logado = true;
            return true;
        } else {
            $this->Logado = false;
            return FALSE;
        }
    }

    public function getNoAccess($idAcesso) {
        $s = new Select();
        // $query = "SELECT ac.nomearquivo FROM admin.painel_no_access as no, admin.painel_acesso as ac WHERE no.acessoid = ac.id AND no.classeacessoid = '" . $idAcesso . "'";

        return $s->SelectQuery($query);
        //return $s->SelectP(['acessoid'], 'no_access', ["classeacessoid", "=", $_SESSION['frlIdCa']]);
    }

    public function Gravar($d = null, $img = null) {
        try {
            $s = new Select();
            $error = array();

            // var_dump($d);
            //Verifica se a acao é para excluir
            if ($d['acao'] == 'excluir') {
                $e = new Delete();
                $cone = new Database();
                $trans = $cone->Begin();
                if ($e->Deletar($trans, $this->tbl, ['id', '=', $d['idRegistro']])) {
                    array_push($error, $this->mensagens->Msg("exSucesso"));
                    $cone->Commit();
                } else {
                    array_push($error, $this->mensagens->Msg("regNPSE"));
                    $cone->Rollback();
                }
                echo json_encode($error);
                exit();
            }


            ///----- Validação ------
            if (empty($d['nome']) && $d['nome'] == "") {
                array_push($error, $this->mensagens->Msg("nmVazio"));
            }
            if (empty($d['email']) && $d['email'] == "") {
                array_push($error, $this->mensagens->Msg("emailVazio"));
            }

            //Validação da senha
            if ($d['acao'] == 'novo' || $d['senha'] <> "") {
                if (empty($d['senha']) && $d['senha'] == "") {
                    array_push($error, $this->mensagens->Msg("senhaVazia"));
                }

                if (isset($d['confirmar']) && isset($d['senha'])) {
                    if ($d['senha'] != $d['confirmar']) {
                        array_push($error, $this->mensagens->Msg("senhaNaoConferem"));
                    }
                }
            }
            if ($d['acao'] == 'editar' && $d['senha'] == "") {
                unset($d['senha']);
            } else {
                //Criptografa a senha
                $d['senha'] = md5($d['senha']);
            }

            if (isset($d['classeacessoid']) && $d['classeacessoid'] < 0) {
                array_push($error, $this->mensagens->Msg("SelclasseDeacesso"));
            }

            $d['acao'] == "editar" ? $cond = ['email', '=', $d['email'], "AND", 'id', '<>', $d['idRegistro']] : $cond = ['email', '=', $d['email']];


            isset($d['ativo']) ? $d['ativo'] = 1 : $d['ativo'] = 0;
            $s->Count($this->tbl, $cond) <> 0 ? array_push($error, $this->mensagens->Msg("emailExiste")) : 0;


            //exit();
            // echo $s->Count($this->tbl, $cond);
            // 
            // 
            // Upload da imagem do usuario
            ///Verificação das imgs

            $upload = null;
            $CaminhoImg = null;
            

            if (isset($img["img"]['name']) && $img["img"]['name'] <> '') {
                try {
                    $imagem = new Image("assets/img/theme", "images");


                    $CaminhoImg = $imagem->upload($img['img'], $d['nome']);
                    ////Adiciona no array o campo imagem
                    $d['imagem'] = $CaminhoImg;
                } catch (Exception $e) {
                    array_push($error, $this->mensagens->Msg("arquivoInvalido"));
                }
            }
            
            // sleep(2);
            if (count($error) <> 0) {
                echo json_encode($error);
                exit();
            }
            //exit();
            ///Remove as colunas do array
            $acao = $d['acao'];
            $idRegistro = $d['idRegistro'];
            unset($d['acao']);
            unset($d['idRegistro']);
            unset($d['confirmar']);
            unset($d['img']);

            $nm_colunas = array_keys($d);
            $nm_valores = array_values($d);

            ////Inicia a conexão com o banco de dados
            $cone = new Database();
            $trans = $cone->Begin();

            if ($acao == "editar") {
                $u = new Update();
                $condicao = ['id', '=', $idRegistro];
                if ($u->Atualizar($trans, $nm_colunas, $nm_valores, $this->tbl, $condicao)) {
                    array_push($error, $this->mensagens->Msg('atSucesso'));
                    $cone->Commit();
                } else {
                    array_push($error, $this->mensagens->Msg('erroAtualizar'));
                    $cone->Rollback();
                }
            } if ($acao == "novo") {
                $u = new Insert();
                if ($u->Insere($trans, $nm_colunas, $nm_valores, $this->tbl)) {
                    array_push($error, $this->mensagens->Msg('inSucesso'));
                    $cone->Commit();
                } else {
                    array_push($error, $this->mensagens->Msg('inErro'));
                    $cone->Rollback();
                }
            }

            echo json_encode($error);
        } catch (Exception $exc) {
            array_push($error, $this->mensagens->Msg('inErro'));
            echo json_encode($error);
            exit();
        }
    }

}

?>
